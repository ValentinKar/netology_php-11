<?php 
error_reporting(E_ALL);        // вывести на экран все ошибки

if ( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['name_db']) )  { 

    $host = (string) $_POST['host'];  
    $database = (string) $_POST['name_db']; 
    $user = (string) $_POST['user'];      
    $password = (string) $_POST['password'];    
    $dbport = 3306; 
$pdo = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password, [
  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
]); 

$statement = $pdo->prepare(" 
CREATE TABLE `question` (
`id` INT(7) NOT NULL AUTO_INCREMENT, 
`id_user` INT(7) NOT NULL, 
`id_theme` INT(5), 
`status` TINYINT(1) NOT NULL DEFAULT '0',    -- (ожидает ответа / опубликован / скрыт)
`date_added` datetime NOT NULL,
`question` text NOT NULL,
`date_answer` datetime,
`answer` text,
`stop_word` text,
PRIMARY KEY (id) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `theme` (
`id` INT(5) NOT NULL AUTO_INCREMENT, 
`date_added` datetime NOT NULL,
`description` text NOT NULL,
PRIMARY KEY (id) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
`id` INT(7) NOT NULL AUTO_INCREMENT,
`name` varchar(50) NOT NULL,
`email` varchar(30) NOT NULL,
PRIMARY KEY (id) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `admin` (
`id` INT(6) NOT NULL AUTO_INCREMENT,
`login` varchar(50) NOT NULL,
`password` varchar(255) NOT NULL,
PRIMARY KEY (id) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `admin` (
`id`, 
`login`, 
`password`
) 
VALUES
(1, 
'admin',    
'admin'
);"); 

$statement->execute(); 
echo 'Ура!'; 
die; 
}; 

?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>create table</title>
</head>
<body>

<h1>Установить таблички в базе данных</h1>

<form method="POST">
	<label>Название БД в которую устанавливаются таблички: 
    <input type="text" name="name_db" placeholder="введите название базы данных в которую хотите установить таблички" value="php11_karpaev_diplom" />
    </label>
    <br /><br />
    <label>Хост: 
    <input type="text" name="host" placeholder="введите название хоста" value="localhost" />
    </label>
    <br /><br />
    <label>Пользователь: 
    <input type="text" name="user" placeholder="введите логин пользователя" value="root" />
    </label>
    <br /><br />
    <label>Пароль: 
    <input type="password" name="password" placeholder="введите пароль пользователя" value="" />
    </label>
    <br /><br />
    <input type="submit" name="create" value="Создать таблички" />
</form>

</body>
</html>