<?php 

namespace Controller;         // пространство имен Controller

use Twig_Autoloader; 
use Twig_Loader_Filesystem; 
use Twig_Environment; 

error_reporting(E_ALL);        //вывести на экран все ошибки

abstract class Controller      // контроллер
{ 
    public $memory = 0;  // параметр памяти скрипта
    public $time = 0;  // время выполнения скрипта

    public function __construct() 
    {  // конструктор запускает обработчик ошибок 
        set_error_handler([$this, 'myErrorHadler']); 
        $this->memory = memory_get_usage(); 
        $this->time= microtime(true); 
    } 

    // вывести на экран параметры
    public function displayParameters()  
    {
        echo '<br />время выполнения (seconds): '; 
        echo (microtime(true) - $this->time); 
        echo '<br />память скриптов (bytes): '; 
        echo number_format(memory_get_usage() - $this->memory, 0, '.', ' '); 
        echo '<br />'; 
    }
   
    // метод обработки ошибок
    public function myErrorHadler($errno, $errstr, $errfile, $errline) 
    { 
    if (!(error_reporting() & $errno)) {
        // Этот код ошибки не включен в error_reporting,
        // так что пусть обрабатываются стандартным обработчиком ошибок PHP
        // echo "Неизвестная ошибка. Завершение работы...<br />" . PHP_EOL;
        return false;
    }
    switch ($errno) {

    // case E_PARSE:
    //     break;

    case E_USER_ERROR: 
        echo "<b>Моя ошибка: My ERROR</b> [{$errno}] {$errstr}<br />" . PHP_EOL;
        echo "  Фатальная ошибка в строке {$errline} файла {$errfile}";
        echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />" . PHP_EOL;
        echo "Завершение работы...<br />" . PHP_EOL;
        exit(1);
        break;

    case E_USER_WARNING:   // E_WARNING: 
        echo "<b>Моя ошибка: My WARNING</b> [{$errno}] {$errstr}<br />" . PHP_EOL;
        break;

    case E_USER_NOTICE:   // E_NOTICE: 
        echo "<b>Моя ошибка: My NOTICE</b> [{$errno}] {$errstr}<br />" . PHP_EOL;
        break;

    default:
        echo "Моя ошибка: Неизвестная ошибка: [{$errno}] {$errstr}<br />" . PHP_EOL;
        break;
    }
    // Не запускаем внутренний обработчик ошибок PHP /
    return true;
    }

    public function twig()  // 
    { 
        include_once './Twig/Autoloader.php';   // 
        Twig_Autoloader::register();
        // --- использую шаблонизатор twig --- 
        $loader = new Twig_Loader_Filesystem('./templates');    // Где лежат шаблоны
        $twig = new Twig_Environment($loader, [ 
            'cache' => './tmp/cache', 
            'auto_reload' => false       // true  // !!!!--- ПОСЛЕ ОТЛАДКИ ПОМЕНЯТЬ ---!!!!
        ]);   // Где будут хранится файлы кэша (php файлы)
        return $twig;
    }

    public function view($name) 
    { 
        $template = $this->twig()->loadTemplate(implode("-", $name->name_files) . '.twig');  
        $template->display($name->parameter); 
        // echo $this->twig()->render( implode("-", $name->name_files) . '.twig', $name->parameter );
        $this->displayParameters();
        die; 
    } 

}