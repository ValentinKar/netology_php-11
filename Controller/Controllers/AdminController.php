<?php 

namespace Controller\Controllers; 

use Controller; 
use Model\Models; 
use Library; 
use Library\Modules; 

error_reporting(E_ALL);        //вывести на экран все ошибки

class AdminController extends Controller\Controller implements Library\Modules\InterfaceAdmin   // 
{ 
    public function isAuthorize($name_table)	 // проверка был ли admin авторизован перед переходом по маршруту
    {
            if (!isset($_SESSION)) { 
                session_start(); 
            };
        if (!isset($_SESSION['id_user'])) { 
            $name_table->name_files = ['admin', 'admin', 'authorization'];
            $this->view($name_table); 
        }; 
        return true; 
    }

    public function authorizationAdmin($name_table)   // первоначальная авторизация
    { 
        $_POST['login'] = isset($_POST['login']) ? $_POST['login'] : ''; 
        $_POST['password'] = isset($_POST['password']) ? $_POST['password'] : ''; 
        $login = strip_tags($_POST['login']);
        $password = strip_tags($_POST['password']);
            if (empty($login) && empty($password)) { 
                $this->view($name_table); 
            }; 
                $name_table->name_table1 = $name_table->name_files[1];
                $name_table->parameter1 = [
                    $login, 
                    $password 
                ]; 
        $model = new \Model\Models\AdminModel(); 
        $request = $model->modelAuthorizationAdmin($name_table); 
        if ($request) { 
            session_start(); 
            $_SESSION['id_user'] = $request; 
            $name_table->name_files = ['admin', 'list']; 
        } 
        else {
            $name_table->parameter = ['echo' => 'Неправильно введен логин или пароль']; 
        }; 
        $this->view($name_table); 
    } 

    public function viewAdmin($name_table)   // просмотр списка администраторов
    { 
        $this->isAuthorize($name_table); 
        $name_table->name_table1 = $name_table->name_files[1]; 
        $name_table->parameter1 = ['login', 'password']; 
            $model = new \Model\Models\AdminModel(); 
            $request = $model->modelView($name_table); 
            if ($request) { 
                $this->view($name_table); 
            }; 
    } 

    public function addAdmin($name_table)   // добавление новых администраторов
    { 
        $_POST['login_new'] = isset($_POST['login_new']) ? $_POST['login_new'] : ''; 
        $_POST['password_new'] = isset($_POST['password_new']) ? $_POST['password_new'] : ''; 
        $login_new = strip_tags($_POST['login_new']);
        $password_new = strip_tags($_POST['password_new']);

        $this->isAuthorize($name_table); 
            if (empty($login_new)) { 
                $name_table->parameter = []; 
                $this->view($name_table); 
            }; 
        // $_POST['password_new'] = isset($_POST['password_new']) ? $_POST['password_new'] : ''; 
            $name_table->name_table1 = $name_table->name_files[1];
            $name_table->parameter1 = [
                $login_new, 
                $password_new
            ]; 
                $model = new \Model\Models\AdminModel(); 
                $request = $model->modelAddAdmin($name_table); 
            if ($request) { 
                $this->view($name_table); 
            }; 
    } 

    public function editAdmin($name_table)   // замена поролей администраторов
    {
        $_POST['password_edit'] = isset($_POST['password_edit']) ? $_POST['password_edit'] : ''; 
        $password_edit = strip_tags($_POST['password_edit']);
        $this->isAuthorize($name_table); 
            if (count($name_table->name_files) !== 4) { 
                $name_table->name_files = ['admin', 'list']; 
                $name_table->parameter = []; 
                $this->view($name_table); 
            }; 
            $name_table->name_table1 = $name_table->name_files[1]; 
            $name_table->parameter1[0] = array_pop($name_table->name_files); 
            $name_table->parameter1[1] = 'login'; 
            $name_table->parameter1[2] = 'password'; 

                $model = new \Model\Models\AdminModel(); 
            if (!empty($password_edit)) { 
                $name_table->parameter1[1] = $password_edit; 
                $request = $model->modelEditAdminRepassword($name_table); 
            }; 
        $request = !empty($password_edit) ? $request : $model->modelParameterView($name_table); 
            if ($request) { 
                $this->view($name_table); 
            }; 
    }

    public function deleteAdmin($name_table)      // удаление администраторов
    {
        $_POST['action'] = isset($_POST['action']) ? $_POST['action'] : ''; 
        $action = strip_tags($_POST['action']);
            $this->isAuthorize($name_table); 
            if (count($name_table->name_files) !== 4) { 
                $name_table->name_files = ['admin', 'list']; 
                $name_table->parameter = []; 
                $this->view($name_table); 
            }; 
        $name_table->name_table1 = $name_table->name_files[1]; 
        $name_table->parameter1[0] = array_pop($name_table->name_files); 
        $name_table->parameter1[1] = 'login'; 
        $name_table->parameter1[2] = 'password'; 

            $model = new \Model\Models\AdminModel(); 
        if (!empty($action) && $action === 'delete_admin') { 
            $request = $model->modelDeteteAdmin($name_table); 
        }; 
        $request = !empty($action) ? $request : $model->modelParameterView($name_table); 
            if ($request) { 
                $this->view($name_table); 
            }; 
    }

    public function viewTheme($name_table)      // просмотр списка тем с кол-вом вопросов (всего\опубликованных\ожидающих ответа) 
    { 
        $this->isAuthorize($name_table); 
            $model = new \Model\Models\AdminModel(); 
            $request = $model->modelViewTheme($name_table); 
        if ($request) { 
            $this->view($name_table); 
        }; 
    } 

    public function createTheme($name_table)      // создание новой темы
    {
        $_POST['theme_new'] = isset($_POST['theme_new']) ? $_POST['theme_new'] : ''; 
        $theme_new = strip_tags($_POST['theme_new']);
        $this->isAuthorize($name_table); 
            if (!empty($theme_new)) { 
                $name_table->name_table1 = $name_table->name_files[1];
                $name_table->parameter1 = $theme_new; 
                    $model = new \Model\Models\AdminModel(); 
                    $request = $model->modelCreateTheme($name_table); 
            }; 
        $this->view($name_table); 
    } 

    public function deleteTheme($name_table)    // удаление темы
    { 
        $_POST['action'] = isset($_POST['action']) ? $_POST['action'] : ''; 
        $action = strip_tags($_POST['action']);
        $this->isAuthorize($name_table); 
            if (count($name_table->name_files) !== 4) { 
                $name_table->name_files = ['admin', 'list']; 
                $name_table->parameter = []; 
                $this->view($name_table); 
            }; 
        $name_table->name_table1 = $name_table->name_files[1]; 
        $name_table->parameter1[0] = array_pop($name_table->name_files); 
        $name_table->parameter1[1] = 'date_added'; 
        $name_table->parameter1[2] = 'description'; 

                $model = new \Model\Models\AdminModel(); 
            if (!empty($action) && $action === 'delete_theme') { 
                $request = $model->modelDeteteTheme($name_table); 
            }; 
        $request = !empty($action) ? $request : $model->modelParameterView($name_table); 
            if ($request) { 
                $this->view($name_table); 
            }; 
    } 

    public function viewQuestion($name_table)  		  // просматривать вопросы в каждой теме. По каждому вопросу видно дату создания, статус (ожидает ответа / опубликован / скрыт)
    {
        $this->isAuthorize($name_table); 
            if (count($name_table->name_files) !== 4) { 
                $name_table->name_files = ['admin', 'list']; 
                $name_table->parameter = []; 
                $this->view($name_table); 
            }; 
        $name_table->name_table1 = $name_table->name_files[1]; 
        $name_table->parameter1 = array_pop($name_table->name_files); 

            $model = new \Model\Models\AdminModel(); 
        $request = $model->modelQuestionView($name_table); 
        if ($request) { 
            $this->view($name_table); 
        }; 
    } 

    public function deleteQuestion($name_table)     // удаление вопроса из темы
    {
        $_POST['action'] = isset($_POST['action']) ? $_POST['action'] : ''; 
        $action = strip_tags($_POST['action']);
        $this->isAuthorize($name_table); 
            if (count($name_table->name_files) !== 4) { 
                $name_table->name_files = ['admin', 'list']; 
                $name_table->parameter = []; 
                $this->view($name_table); 
            }; 
        $name_table->name_table1 = $name_table->name_files[1]; 
        $name_table->parameter1[0] = array_pop($name_table->name_files); 
        $name_table->parameter1[1] = 'question'; 
        $name_table->parameter1[2] = 'status'; 

            $model = new \Model\Models\AdminModel(); 
        if (!empty($action) && $action === 'delete_question') { 
            $request = $model->modelDeleteQuestion($name_table); 
        }; 
    $request = !empty($action) ? $request : $model->modelParameterView($name_table); 
        if ($request) { 
            $this->view($name_table); 
        }; 
    } 

    public function editQuestion($name_table)      // скрывать опубликованные вопросы, публиковать скрытые вопросы.
    // Редактировать автора, текст вопроса и текст ответа. 
    // Перемещать вопрос из одной темы в другую. 
    { 
        $_POST['action'] = isset($_POST['action']) ? $_POST['action'] : ''; 
        $_POST['autor'] = isset($_POST['autor']) ? $_POST['autor'] : ''; 
        $_POST['status'] = isset($_POST['status']) ? $_POST['status'] : ''; 
        $_POST['question'] = isset($_POST['question']) ? $_POST['question'] : ''; 
        $_POST['answer'] = isset($_POST['answer']) ? $_POST['answer'] : ''; 
        $_POST['theme'] = isset($_POST['theme']) ? $_POST['theme'] : '';
            $action = strip_tags($_POST['action']);
            $autor = strip_tags($_POST['autor']);
            $status = strip_tags($_POST['status']); 
            $question = strip_tags($_POST['question']);
            $answer = strip_tags($_POST['answer']);
            $theme = strip_tags($_POST['theme']);

        $this->isAuthorize($name_table); 
            if (count($name_table->name_files) !== 4) { 
                $name_table->name_files = ['admin', 'successfully']; 
                $name_table->parameter = [
                'echo' => 'не определен номер вопроса для редактирования'
                ]; 
                $this->view($name_table); 
            }; 
        $name_table->name_table1 = $name_table->name_files[1]; 
        $name_table->parameter1[0] = array_pop($name_table->name_files); 

            $model = new \Model\Models\AdminModel(); 

        if (!empty($action) && 
            $action === 'edit_question' && 
            !empty($autor) && 
            !empty($status) && 
            !empty($question)) 
        :  
            // $_POST['answer'] = isset($_POST['answer']) ? $_POST['answer'] : ''; 
            // $_POST['theme'] = isset($_POST['theme']) ? $_POST['theme'] : ''; 
                $name_table->parameter1[1] = $question; 
                $name_table->parameter1[2] = $answer; 
                $name_table->parameter1[3] = $theme;  
                $name_table->parameter1[4] = $autor;  
                $name_table->parameter1[5] = $status;  
            $request = $model->modelEditQuestion($name_table); 
        endif; 

        $request = !empty($action) ? $request : $model->modelEditQuestionView($name_table); 
            if ($request) { 
                $this->view($name_table); 
            }; 
    } 

    public function addAnswer($name_table)      // добавление ответа на вопрос с публикацией на сайте, либо со скрытием вопроса 
    { 
        $_POST['answer'] = isset($_POST['answer']) ? $_POST['answer'] : ''; 
        $_POST['status'] = isset($_POST['status']) ? $_POST['status'] : '';
            $answer = strip_tags($_POST['answer']);
            $status = strip_tags($_POST['status']);

        $this->isAuthorize($name_table); 
            if (count($name_table->name_files) !== 4) { 
                $name_table->name_files = ['admin', 'successfully']; 
                $name_table->parameter = [
                    'echo' => 'не определен номер вопроса для ответа на него' 
                ]; 
                $this->view($name_table); 
            }; 
        $name_table->name_table1 = 'question'; 
        $name_table->parameter1[0] = array_pop($name_table->name_files); 

        $model = new \Model\Models\AdminModel(); 
            if (!empty($status) && 
                !empty($answer)) 
            { 
                $name_table->parameter1[1] = $answer; 
                $name_table->parameter1[2] = $status; 
                $request = $model->modelAddAnswer($name_table); 
            } 
            else { 
                $request = $model->modelEditQuestionView($name_table); 
            }; 
        if ($request) { 
            $this->view($name_table); 
        }; 
    } 

    public function viewAnswer($name_table)         // видеть список всех вопросов без ответа во всех темах в порядке их добавления. И иметь возможность их редактировать и удалять.
    { 
        $this->isAuthorize($name_table); 
            $model = new \Model\Models\AdminModel(); 
            $request = $model->modelAnswerView($name_table); 
        if ($request) { 
            $this->view($name_table); 
        }; 
    } 

}   // завершение класса AdminController