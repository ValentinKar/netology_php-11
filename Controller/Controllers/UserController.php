<?php 

namespace Controller\Controllers; 

use Controller; 
use Model\Models; 

error_reporting(E_ALL);        //вывести на экран все ошибки

class UserController extends Controller\Controller 
{ 

    // проверка пользовательского Email - адреса с помощью регулярных выражений
    public function emailVerification($email)  
    { 
        $search = '/^([a-zA-Z0-9а-яёА-ЯЁ\\.\\-_]{1,})@([a-zA-Z0-9а-яёА-ЯЁ\\_\\-]{2,255}).([a-zA-Z0-9а-яёА-ЯЁ]{2,6})$/u';
            if (!preg_match($search, $email))  {
                // var_dump(preg_match($search, $email)); 
                return false;
            }; 
        return true;
    } 

    public function addQuestion($name_table)  // добавление вопроса пользователя
    { 
        $id_theme = strip_tags($_POST['id_theme']);
        $user_name = strip_tags($_POST['user_name']);
        $user_email = strip_tags($_POST['user_email']);
        $user_question = strip_tags($_POST['user_question']);
            if (empty($user_email) && empty($user_question)) { 
                $name_table->parameter = [
                    'echo' => 'не указан email или вопрос', 
                    'id_theme' => $id_theme
                    ]; 
                $this->view($name_table); 
            }; 
                // проверка Email пользователя 
                if (!$this->emailVerification($user_email))  {  
                    $name_table->parameter = [ 
                        'echo' => 'введен неправильный email', 
                        'id_theme' => $id_theme 
                    ]; 
                    $this->view($name_table); 
                }; 

            $name_table->name_table1 = $name_table->name_files[0];
            $name_table->parameter1 = [
                $user_name, 
                $user_email
            ]; 
                $name_table->name_table2 = $name_table->name_files[1]; 
                $name_table->parameter2 = [
                    $id_theme, 
                    $user_question
                ]; 
            $model = new \Model\Models\UserModel(); 
            $request = $model->modelAddQuestion($name_table); 
            if ($request) { 
                $name_table->name_files = ['question', 'successfully']; 
                $this->view($name_table); 
            }; 
    } 

    public function viewQuestion($name_table)  // показ вопросов пользователю
    { 
        $model = new \Model\Models\UserModel(); 
        $request = $model->modelViewQuestion($name_table); 
        if ($request) { 
            $this->view($name_table); 
        }; 
    } 

    public function viewTheme($name_table)    // показ тем пользователю
    {
        $model = new \Model\Models\UserModel(); 
        $request = $model->modelView($name_table); 
        if ($request) { 
            $this->view($name_table); 
        }; 
    }

}   // завершение класса UserController