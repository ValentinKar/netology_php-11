<?php 

namespace Library\Modules; 

error_reporting(E_ALL);        // вывести на экран все ошибки

interface InterfaceAdmin      // интерфейс используется классом AdminController
{ 
    public function authorizationAdmin($name_table);   // авторизация
    public function viewAdmin($name_table);   // просмотр списка администраторов
    public function addAdmin($name_table);   // добавление новых администраторов
    public function editAdmin($name_table);   // замена поролей администраторов
    public function deleteAdmin($name_table);   // удаление администраторов
    public function viewTheme($name_table);   // просмотр списка тем с кол-вом вопросов (всего\опубликованных\ожидающих ответа) 
    public function createTheme($name_table);   // создание новой темы
    public function deleteTheme($name_table);   // удаление темы
    public function viewQuestion($name_table);  // просматривать вопросы в каждой теме. По каждому вопросу видно дату создания, статус (ожидает ответа / опубликован / скрыт)
    public function deleteQuestion($name_table);   // удаление вопроса из темы
    public function editQuestion($name_table);   //  скрывать опубликованные вопросы, публиковать скрытые вопросы.
    // Редактировать автора, текст вопроса и текст ответа. 
    // Перемещать вопрос из одной темы в другую. 
    public function addAnswer($name_table);   // добавление ответа на вопрос с публикацией на сайте, либо со скрытием вопроса
    public function viewAnswer($name_table);  // видеть список всех вопросов без ответа во всех темах в порядке их добавления. И иметь возможность их редактировать и удалять.
}