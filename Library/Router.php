<?php 

namespace Library;           // пространство имен Library

use Controller\Controllers; 
use Library\Objects; 

error_reporting(E_ALL);        //вывести на экран все ошибки

class Router       // класс роутер
{ 

    public function routerLevel1()     //
    { 
        $path = preg_split('/\//', $_SERVER['REQUEST_URI'], 0, PREG_SPLIT_NO_EMPTY);
            $position_question = array_search('?', $path); 
                if (array_search('index.php', $path)) {
                    $position_question = array_search('index.php', $path); 
                }; 
        $path_list = array_slice($path, $position_question); 
        array_shift($path_list); 

                if (count($path_list) < 2) {
                    $path_list = ['user', 'view'];
                }; 
                if (array_pop($path) === 'index.php' || array_pop($path) === 'localhost' || empty(array_pop($path))) {
                    $path_list = ['user', 'view'];
                }; 
            $address = new \Library\Objects\NameFile(); 
            $address->name_files = $path_list; 
            $address->parameter = []; 
        $controllerText = '\Controller\Controllers\\' . ucfirst($address->name_files[0]) . 'Controller';
        if (class_exists($controllerText)) { 
            $request_controller = new $controllerText(); 
        };

        if (empty($_POST) && $address->name_files[0] === 'user')  :  
            if ( $address->name_files[1] === 'view' ) { 
                $request_controller->viewQuestion($address); 
            }; 
            if ($address->name_files[1] === 'theme') { 
                $address->name_table1 = $address->name_files[1]; 
                $address->parameter1 = [ 'date_added', 'description' ];
                $request_controller->viewTheme($address); 
            }; 
            if ($address->name_files[2] === 'add') { 
                $id = ''; 
                $id = empty($address->name_files[3]) ? $id : array_pop($address->name_files); 
                $address->parameter = ['id_theme' => $id ]; 
                $request_controller->view($address); 
            }; 
        endif; 

            if (count($address->name_files) >= 3)  {
                $path_list = ['user', 'view'];
                // имя метода в объекте класса UserController или AdminController
                $methodText = $address->name_files[2] . ucfirst($address->name_files[1]); 
            }
            else  { 
                $methodText = 'nothing'; 
            }; 
        if (method_exists($request_controller, $methodText)) {
            $request_controller->$methodText($address); 
        }; 
        $request_controller->view($address); 
    }

}   // завершение класса Router