<?php 

namespace Library\Objects; 

error_reporting(E_ALL);        //вывести на экран все ошибки

// обьект класса NameFile передается в контроллер в качестве параметра 
class NameFile 
{ 
    public $name_files;   // аргумент хранения имени шаблона .twig и названия методов класса Controller
    public $parameter;   // аргумент для хранения переменных, которые передаются в шаблон .twig 

    public $name_table1;  // аргумент для хранения названия табличек, с которыми работает SQL  
    public $parameter1;   // аргумент для хранения параметров, с которыми работает SQL  
    public $name_table2;  // аргумент для хранения названия табличек, с которыми работает SQL  
    public $parameter2;    // аргумент для хранения параметров, с которыми работает SQL  
} 