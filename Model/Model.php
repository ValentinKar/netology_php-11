<?php 

namespace Model;         // пространство имен Model

use PDO;

error_reporting(E_ALL);        //вывести на экран все ошибки

abstract class Model      // модель
{ 

    public function connect()       // соединение с базой данных
    { 
        // include_once './index.php'; 
        $host = HOST;  
        $database = DATABASE; 
        $user = USER; 
        $password = PASSWORD; 
        $dbport = DBPORT; 

        $pdo = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password, [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ]); 
        return $pdo; 
    } 

    public function modelView($name_table)   // 
    { 
            $pdo = $this->connect(); 
            $table = $name_table->name_table1; 
            $param_1 = $name_table->parameter1[0]; 
            $param_2 = $name_table->parameter1[1]; 

        $statement = $pdo->prepare("SELECT * FROM $table"); 
        $statement->execute(); 

            $array = []; 
            foreach ($statement as $row)  {  
                $array[] = [ 
                    'id' => $row['id'] , 
                    $param_1 => htmlspecialchars($row[$param_1]), 
                    $param_2 => htmlspecialchars($row[$param_2]) 
                ]; 
            }; 

        $name_table->parameter = ['array' => $array]; 
        return true; 
    } 

    public function modelQuestion($name_table)  // итог - запрос в базу данных в таблицу question
    { 
        $table1 = (string) $name_table->name_table1;   // question
        $id = (integer) $name_table->parameter1[0];    // id 
            $statement= $this->connect()->prepare("SELECT 
                q.status, 
                q.date_added,
                q.question, 
                -- q.date_answer,
                q.answer, 
                t.description AS theme_quest, 
                q.id_theme AS id_theme_quest, 
                u.name AS user_name, 
                u.email AS user_email, 
                q.id_user AS id_user_quest, 
                q.id AS id_quest
                FROM $table1 AS q
                JOIN theme AS t ON t.id = q.id_theme 
                JOIN user AS u ON u.id = q.id_user 
                HAVING id_quest = ? 
                ;"); 
            $statement->execute(["{$id}"]); 
            $result = $statement->fetch(); 
        return $result;
    }

}   // завершение класса Model