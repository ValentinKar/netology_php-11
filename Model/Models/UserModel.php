<?php 

namespace Model\Models; 

use Model; 

error_reporting(E_ALL);        // вывести на экран все ошибки

class UserModel extends Model\Model 
{

    public function modelAddQuestion($name_table)  // итог - добавление вопроса в таблицу question($table2) и добавление пользователя в таблицу user($table1) если такого пользователя там не было
    { 
        $pdo = $this->connect(); 
            $table1 = (string) $name_table->name_table1; 
            $name = (string) $name_table->parameter1[0]; 
            $email = (string) $name_table->parameter1[1]; 
        $table2 = (string) $name_table->name_table2; 
        $id_theme = (integer) $name_table->parameter2[0]; 
        $question = (string) $name_table->parameter2[1]; 

            $sth= $pdo->prepare("SELECT id, email FROM $table1 WHERE email=?;"); 
            $sth->execute(["{$email}"]);
            $result = $sth->fetch();

        if ($result) {    // проверяю уникальнось пользователя по email
            $id_user = $result["id"]; 
        }
        else {    // если пользователя с указанным email нет в базе
            $statement = $pdo->prepare("
            INSERT INTO $table1 (name, email)
            VALUES (?, ?); "); 
            $statement->execute(["{$name}", "{$email}"]);  
            $id_user = $pdo->lastInsertId(); 
        }; 
                $statement = $pdo->prepare("
                INSERT INTO $table2 (id_user, id_theme, status, date_added, question)
                VALUES (?, ?, 1, now(), ?); "); 
                $statement->execute(["{$id_user}", "{$id_theme}", "{$question}"]);  
        return true; 
    }

    public function modelViewQuestion($name_table)   // итог - массив вопросов для показа пользователю
    {
        $pdo = $this->connect(); 
                $questions = []; 
                $sth = $pdo->prepare("SELECT id, description FROM theme;"); 
                $sth->execute(); 

        foreach ($sth as $theme)  :  
            $id = $theme['id']; 
                $statement = $pdo->prepare("SELECT 
                    q.question,        -- 
                    q.answer,         --  
                    q.status,  
                    q.id_theme AS them 
                     FROM question AS q  
                     JOIN user AS u ON u.id=q.id_user 
                     HAVING q.status = 2 AND them = ? 
                    ;"); 
                $statement->execute(["{$id}"]); 
            $questions_arr = []; 
                foreach ($statement as $row)  {  
                    $questions_arr[] = [
                        'question' => htmlspecialchars($row['question']), 
                        'answer' => htmlspecialchars($row['answer']) 
                    ];
                }; 

                $questions[] = [ 
                    'theme' => $theme['description'], 
                    'id_theme' => $id, 
                    'array' => $questions_arr 
                ];
            unset($questions_arr); 
        endforeach; 
        $name_table->parameter = ['questions' => $questions]; 
        return true; 
    }

}    // завершение класса UserModel