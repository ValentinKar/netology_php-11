<?php 

namespace Model\Models; 

use Model; 

error_reporting(E_ALL);        //вывести на экран все ошибки

class AdminModel extends Model\Model 
{
    private $salt = 'ibtfnwhcpq';  // аргумент, усложненяющий пароль администратора

    // усложнение пароля
    function getSalt() 
    {
        return $this->salt;
    } 

    // кодирование пароля
    function getPassword($password)  
    {
        return md5((string)$password . $this->getSalt());
    }

    public function modelAuthorizationAdmin($name_table)  // итог - авторизация администратора
    { 
        $table1 = (string) $name_table->name_table1;   // admin
        $login = (string) $name_table->parameter1[0];   // login
        $password = $this->getPassword($name_table->parameter1[1]); // password
                $sth = $this->connect()->prepare("SELECT * FROM $table1 WHERE login=? AND password=?;"); 
                $sth->execute(["{$login}", "{$password}"]); 
                $result = $sth->fetch(); 
        if ($result) {   // проверяю пароль и логин администратора
            return $result['id'];
        } 
        else {    // если пароль и логин введены неправильно
            return false;
        }; 
    }

    public function modelAddAdmin($name_table)  // итог - добавление нового администратора в базу данных (таблица admin)
    { 
        $table1 = (string) $name_table->name_table1;    // admin
        $login_new = (string) $name_table->parameter1[0];  // login
        $password_new = $this->getPassword($name_table->parameter1[1]);  // password

            $statement = $this->connect()->prepare("
                INSERT INTO $table1 (login, password)
                VALUES ( ?, ? ); "); 
            $statement->execute(["{$login_new}", "{$password_new}"]);  
        unset($name_table->name_files, $name_table->name_table1, $name_table->parameter1); 
        $name_table->name_files = ['admin', 'successfully']; 
        $name_table->parameter = ['echo' => 'Новый администратор добавлен в базу данных']; 
        return true; 
    }

    public function modelParameterView($name_table)  // итог - вывод на экран учетной записи конкретного администратора password, login для редактирования пароля
    { 
        $table1 = (string) $name_table->name_table1;   // admin
        $id = (integer) $name_table->parameter1[0];    // id
        $param1 = (string) $name_table->parameter1[1]; 
        // $param2 = (string) $name_table->parameter1[2]; 
                $sth= $this->connect()->prepare("SELECT * FROM $table1 WHERE id=?;"); 
                $sth->execute(["{$id}"]); 
                $result = $sth->fetch();  
        if ($result) { 
            $name_table->parameter = [ 
            'id' => $result['id'], 
            $param1 => htmlspecialchars($result[$param1]), 
            // $param2 => htmlspecialchars($result[$param2]) 
            ]; 
            return true; 
        } 
        else {   
            return false;
        }; 
    }

    public function modelEditAdminRepassword($name_table)  // итог - внесение в базу данных отредактированного (измененного) пароля администратора 
    { 
        $table1 = (string) $name_table->name_table1;  // admin
        $id_edit = (integer) $name_table->parameter1[0];  // id
        $password_edit = $this->getPassword($name_table->parameter1[1]); // password
            $statement = $this->connect()->prepare("UPDATE $table1 SET password = ? WHERE id = ?;"); 
            $statement->execute(["{$password_edit}", "{$id_edit}"]); 
        unset($name_table->name_files, $name_table->name_table1, $name_table->parameter1); 
        $name_table->name_files = ['admin', 'successfully']; 
        $name_table->parameter = ['echo' => 'Пароль администратора был изменен']; 
        return true; 
    } 

    public function modelDeteteAdmin($name_table)  // итог - удаление учетной записи администратора
    { 
        $table1 = (string) $name_table->name_table1;   // admin
        $id_delete = (integer) $name_table->parameter1[0];  // id
            $statement = $this->connect()->prepare("DELETE FROM $table1 WHERE id=?;");  
            $statement->execute(["{$id_delete}"]);   
        unset( $name_table->name_files, $name_table->name_table1, $name_table->parameter1 );
        $name_table->name_files = ['admin', 'successfully']; 
        $name_table->parameter = ['echo' => 'Учетная запись администратора была успешно удалена из базы данных']; 
        return true; 
    }

    public function modelViewTheme($name_table)  // итог - вывод списка тем с кол-вом вопросов (всего\ожидающих ответа\опубликованных)
    {
        $pdo = $this->connect(); 
        $themes = []; 
                $sth= $pdo->prepare("SELECT id, date_added, description FROM theme;"); 
                $sth->execute(); 

    foreach ($sth as $theme)  :  
    $id = $theme['id']; 

            $statement = $pdo->query("SELECT COUNT(*) FROM question WHERE id_theme = '{$id}';");   // всего вопросов
            $result = $statement->fetch(); 
                $total = $result[0]; 

                $statement = $pdo->query("SELECT COUNT(*) FROM question WHERE status = 1 AND id_theme = '{$id}';");   // ожидающие ответа
                $result = $statement->fetch(); 
                $waiting_answered = $result[0]; 

        $statement = $pdo->query("SELECT COUNT(*) FROM question WHERE status = 2 AND id_theme = '{$id}';");   // опубликованные вопросы
        $result = $statement->fetch(); 
        $published = $result[0]; 

            $statement = $pdo->query("SELECT COUNT(*) FROM question WHERE status = 3 AND id_theme = '{$id}';");   // скрытые вопросы
            $result = $statement->fetch(); 
            $hidden = $result[0]; 

        $themes[] = [ 
            'id_theme' => $id, 
            'theme' => htmlspecialchars($theme['description']), 
            'date_added' => $theme['date_added'],
            'total' => $total, 
            'waiting_answered' => $waiting_answered,  // ожидающие ответа
            'published' => $published,         // опубликованные вопросы
            'hidden' => $hidden              // скрытые вопросы
        ]; 
    endforeach; 
    $name_table->parameter = ['themes' => $themes]; 
    return true; 
    }

    public function modelCreateTheme($name_table)  // итог - добавление новой темы в базу данных
    { 
        $table1 = (string) $name_table->name_table1;    // theme
        $description = (string) $name_table->parameter1;  // description
            $statement = $this->connect()->prepare("
            INSERT INTO $table1 (date_added, description)
            VALUES (now(), ?);"); 
            $statement->execute(["{$description}"]);  
        unset($name_table->name_files, $name_table->name_table1, $name_table->parameter1); 
        $name_table->name_files = ['admin', 'successfully']; 
        $name_table->parameter = ['echo' => 'Новая тема добавлена в базу данных']; 
        return true; 
    } 

    public function modelDeteteTheme($name_table)  // итог - удаление темы 
    { 
        $table1 = (string) $name_table->name_table1;   // theme
        $id = (integer) $name_table->parameter1[0];  // id
        $pdo = $this->connect(); 
            $statement = $pdo->prepare("DELETE FROM question WHERE id_theme = ?;"); 
            $statement->execute(["{$id}"]); 

                $statement = $pdo->prepare("DELETE FROM $table1 WHERE id = ?;"); 
                $statement->execute(["{$id}"]);   

        unset($name_table->name_files, $name_table->name_table1, $name_table->parameter1); 
        $name_table->name_files = ['admin', 'successfully']; 
        $name_table->parameter = ['echo' => 'Тема была успешно удалена из базы данных']; 
        return true; 
    } 

    public function modelQuestionView($name_table)  // итог - вывод на экран всех вопросов по теме с id
    { 
        $table1 = (string) $name_table->name_table1;   // question
        $id = (integer) $name_table->parameter1;      // id

            $pdo = $this->connect(); 
            $sth = $pdo->prepare("SELECT description FROM theme WHERE id=?;"); 
            $sth->execute(["{$id}"]); 
            $result = $sth->fetch(); 
            $theme = $result[0]; 

        $sth = $pdo->prepare("SELECT id, question, answer, date_added, status FROM $table1 WHERE id_theme=?;"); 
        $sth->execute(["{$id}"]); 
 
        $questions = []; 
            foreach ( $sth as $row ) { 
                $questions[] = [
                    'id' => $row['id'], 
                    'question' => htmlspecialchars( $row['question'] ), 
                    'answer' => htmlspecialchars( $row['answer'] ), 
                    'date_added' => $row['date_added'],
                    'status' => $row['status']
                ]; 
            }; 

        $name_table->parameter = [ 
            'theme' => $theme, 
            'questions' => $questions 
        ]; 
        return true; 
    } 

  public function modelDeleteQuestion($name_table)  // итог - удаление вопроса
  { 
    $table1 = (string) $name_table->name_table1;  // question
    $id = (integer) $name_table->parameter1[0];  // id 
          $statement = $this->connect()->prepare("DELETE FROM $table1 WHERE id = ?;"); 
          $statement->execute(["{$id}"]);   
    unset($name_table->name_files, $name_table->name_table1, $name_table->parameter1); 
    $name_table->name_files = ['admin', 'successfully']; 
    $name_table->parameter = ['echo' => 'Вопрос был успешно удален из базы данных']; 
    return true; 
  } 

    public function modelEditQuestionView($name_table)  // итог - редактирование конкретного вопроса по конкретной теме (текста вопроса, ответа, изменение темы, автора)
    { 
        $result = $this->modelQuestion($name_table); 
        if ($result) { 
                $name_table->name_table1 = 'theme'; 
                $name_table->parameter1[0] = 'date_added'; 
                $name_table->parameter1[1] = 'description'; 
                $this->modelView($name_table); 
                $themes = $name_table->parameter; 
                    $name_table->name_table1 = 'user'; 
                    $name_table->parameter1[0] = 'name'; 
                    $name_table->parameter1[1] = 'email'; 
                    $this->modelView($name_table); 
                    $autors = $name_table->parameter; 
                    // unset($name_table->parameter); 
            $name_table->parameter = [ 
              'theme' => $result['theme_quest'], 
              'id' => $result['id_quest'], 
              'question' => htmlspecialchars($result['question']), 
              'answer' => htmlspecialchars($result['answer']),
              'status' => $result['status'], 
              'user_name' => htmlspecialchars($result['user_name']), 
              'user_email' => htmlspecialchars($result['user_email']), 
              'date_added' => $result['date_added'], 
              'id_theme_quest' => $result['id_theme_quest'], 
              'id_user_quest' => $result['id_user_quest'], 
              'themes' => $themes, 
              'autors' => $autors
            ]; 
            return true; 
        } 
        else { 
            return false;
        }; 
    }

    public function modelEditQuestion($name_table)  // итог - 
    { 
                $table1 = (string) $name_table->name_table1; // question
                $id = (string) $name_table->parameter1[0];  // id
                $question = (string) $name_table->parameter1[1]; 
                $answer = (string) $name_table->parameter1[2]; 
                $theme = (integer) $name_table->parameter1[3]; 
                $autor = (integer) $name_table->parameter1[4]; 
                $status = (integer) $name_table->parameter1[5]; 
        $statement = $this->connect()->prepare("UPDATE $table1 SET id_user = ?, id_theme = ?, status = ?, question = ?, answer = ?  WHERE id = ?;"); 
        $statement->execute([ 
            "{$autor}", 
            "{$theme}", 
            "{$status}", 
            "{$question}", 
            "{$answer}", 
            "{$id}" 
        ]); 
            unset($name_table->name_files, $name_table->name_table1, $name_table->parameter1); 
        $name_table->name_files = ['admin', 'successfully']; 
        $name_table->parameter = ['echo' => 'поля выбранного Вами вопроса были отредактированы']; 
        return true; 
    } 

    public function modelAddAnswerView($name_table)     // итог - редактирование конкретного вопроса по конкретной теме (текста вопроса, ответа, изменение темы, автора)
    { 
        $result = $this->modelQuestion($name_table); 
        if ($result) { 
            $name_table->parameter = [ 
                'theme' => $result['theme_quest'], 
                'id' => $result['id_quest'], 
                'question' => htmlspecialchars($result['question']), 
                'answer' => htmlspecialchars($result['answer']),
                'status' => $result['status'], 
                'user_name' => htmlspecialchars($result['user_name']), 
                'user_email' => htmlspecialchars($result['user_email']), 
                'date_added' => $result['date_added'] 
            ]; 
            return true; 
        } 
        else { 
            return false;
        }; 
    }

    public function modelAddAnswer($name_table)  // итог - добавление ответа на вопрос
    { 
        $table1 = (string) $name_table->name_table1;   // question
        $id = (string) $name_table->parameter1[0];     // id
        $answer = (string) $name_table->parameter1[1]; 
        $status = (integer) $name_table->parameter1[2]; 

            $statement = $this->connect()->prepare("UPDATE $table1 SET answer = ?, status = ?, date_answer = now() WHERE id = ?;"); 
            $statement->execute([ 
                "{$answer}", 
                "{$status}", 
                "{$id}" 
            ]); 

        unset($name_table->name_files, $name_table->name_table1, $name_table->parameter1); 
        $name_table->name_files = ['admin', 'successfully']; 
        $name_table->parameter = [ 
            'echo' => 'ответ на вопрос был добавлен в базу данных' 
        ]; 
        return true; 
    } 

    public function modelAnswerView($name_table)  // итог - вывод на экран всех вопросов ожидающих ответа
    { 
        $sth = $this->connect()->prepare("SELECT 
            id, 
            question, 
            answer, 
            date_added 
            FROM question 
            WHERE status = 1 
            -- ORDER BY date_added DESC
            ORDER BY date_added ASC 
        ;"); 
        $sth->execute(); 
 
        $questions = []; 
            foreach ($sth as $row) { 
                $questions[] = [
                    'id' => $row['id'], 
                    'question' => htmlspecialchars($row['question']), 
                    'answer' => htmlspecialchars($row['answer']), 
                    'date_added' => $row['date_added']
                ]; 
            }; 
        $name_table->parameter = [ 
                'questions' => $questions 
        ]; 
        return true; 
    } 

}  // завершение класса AdminModel